from jinja2 import Template, Environment, FileSystemLoader
from flask import request
import json


def view_search(region="na"):
    context = {}

    if request.args.get("name"):
        name = request.args.get("name")
        wild = request.args.get("wild")

        player_grids = []

        grids = json.loads(open("data/{}_pvp_players_dict.json".format(region), "r").read())["grids"]
        for grid in grids:
            for player in grid["players"]:
                if wild:
                    if name in player["name"]:
                        player_grids.append({"name": player["name"], "grid": grid["grid"]})
                else:
                    if name == player["name"]:
                        player_grids.append({"name": player["name"], "grid": grid["grid"]})

        context["results"] = player_grids

    env = Environment(loader=FileSystemLoader("templates/"))
    template = env.get_template("search.html")
    return template.render(context)
