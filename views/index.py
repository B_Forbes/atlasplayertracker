import json
from jinja2 import Template, Environment, FileSystemLoader


def convert_grids(grids):
    """
    Convert from stored dict to easily used by template dict

    :param players_dict:
    :return: dictionary
    """
    r_dict = {}

    for grid in grids["grids"]:
        if grid is not None:
            r_dict[grid["grid"]] = grid
    return r_dict


def view_index(region="na", view=None):
    grid_data = json.loads(open("data/{}_pvp_players_dict.json".format(region), "r").read())
    grid_ships = json.loads(open("data/{}_pvp_ships.json".format(region), "r").read())

    # Convert to an easier-to-use dictionary for template
    t_grids = convert_grids(grid_data)
    online = 0

    labels = {}

    for grid, grid_dict in t_grids.items():
        online += len(grid_dict["players"])
        if "label" in grid_dict.keys():
            labels[grid_dict["grid"]] = grid_dict["label"]

    server = ""
    if region in "na":
        server = "[NA PvP] The Kraken's Maw"

    if region in "eu":
        server = "[EU PvP] The Whale's Wrath"

    context = {
        "player_count": online,
        "grid_letters": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"],
        "grid_numbers": list(range(1, 16)),
        "grid_dict": t_grids,
        "server": server,
        "server_region": region,
        "view": view,
        "labels": labels,
        "latest_update": grid_data["meta"]["last_updated"],
        "grid_ships": grid_ships
    }

    env = Environment(loader=FileSystemLoader("templates/"))

    if view == "ship":
        template = env.get_template("index_ship.html")
    elif view == "player":
        template = env.get_template("index_player.html")
    else:
        template = env.get_template("index_player.html")

    return template.render(context)
