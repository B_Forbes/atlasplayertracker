import json
from jinja2 import Template, Environment, FileSystemLoader


def view_grid(grid, region="na"):
    grids = json.loads(open("data/{}_pvp_players_dict.json".format(region), "r").read())["grids"]
    grid_ships = json.loads(open("data/{}_pvp_ships.json".format(region), "r").read())

    players = []
    ships_on_grid = []
    players_on_ships = 0

    for g in grids:
        if g["grid"] == grid:
            for player in g["players"]:
                players.append(player)

    for ship_list in grid_ships["grids"][grid]["ships"]:
        ships_on_grid.append({
                                "players": ship_list["players"],
                                "source_grid": ship_list["source_grid"]
                            })

        for ship in ship_list:
            players_on_ships += len(ship)

    context = {
        "players": players,
        "ships": ships_on_grid,
        "players_on_ships": players_on_ships,
        "grid": grid,
    }

    env = Environment(loader=FileSystemLoader("templates/"))
    template = env.get_template("grid.html")
    return template.render(context)
