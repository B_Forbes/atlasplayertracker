#Atlas Player Tracker

As I've taken the Atlas Player Tracker offline, I am now fully releasing all the code used to keep it running. This project is no longer being maintained. It is already known that some parts could have been done better, and that some of the code is horribly written. Deal with it :)

##Instructions

These instructions will cover getting the code running in an environment like the one I used:

1. Download repository

2. Install Requirements
    ```
    pip install -r REQUIREMENTS.txt
    ```

    Install the python requirements. I suggest using a virtual environment.
    
3. A slight modification to the message buffer size is needed in the python-valve library. This is my solution that worked well:

    Modify the following file:
    valve/source/__init__.py

    The path will change depending on your python dependency installation process.

    Modify line the following line:
    ```
    data = ready[0][0].recv(1400)
    ```
    to
    ```
    data = ready[0][0].recv(4500)
    ```

    https://github.com/serverstf/python-valve/blob/479768a3cb07f6ae6ba62a7b6308f31aac867607/valve/source/__init__.py#L123

4. Install MongoDB (https://docs.mongodb.com/manual/installation/)
    The application is configured to use a default installation on localhost.

5. Run update.py

     It should start generating some json files in the data/ directory as well as populating the MongoDB. During deployment, just keep this script running at all times.

6. Run main.py

    It's configured to listen on 0.0.0.0:80, so go ahead and connect to localhost:80 to test it.
    The Flask dev server says not to use it in production, but I assure you it's good enough for any small project. For the first few days it was perfectly capable of handling at least ~3 requests per second.


    If there's issues with the instructions above, please let me know in the project Discord: https://discord.gg/VCSDdaC
