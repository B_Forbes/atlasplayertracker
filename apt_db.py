from pymongo import MongoClient
from pprint import pprint
import datetime

MONGO_HOST = "localhost"
MONGO_PORT = 27017


def get_latest_grids(region):
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["grids"]

    grids = []

    for letter in ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"]:
        for i in range(1, 16):
            i = str(i)
            grids.append(collection.find_one({"grid": letter + i}, sort=[("timestamp", -1)]))

    return grids


def get_latest_update(region):
    """
    Get the epoch time of the last completed update for the region
    :param region:
    :return:
    """
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["updates"]

    return collection.find_one({}, sort=[("timestamp", -1)])


def get_player_grids(name, region, time_offset=None):
    """

    :param name: String, player name
    :param region: eu/na
    :param time_offset Within how many seconds of now
    :return:
    """
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["grids"]

    if time_offset:
        time_offset = datetime.datetime.timestamp(datetime.datetime.now() - datetime.timedelta(seconds=time_offset))
    else:
        time_offset = 0

    results = [doc for doc in collection.find({"timestamp": {"$gt": time_offset}, "players": {"$elemMatch": {"name": name}}}, {"grid": 1, "timestamp": 1})]
    return results


def group_in_grid(grid, group, region, time_offset=None):
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["grids"]

    match = []

    if time_offset:
        time_offset = datetime.datetime.timestamp(datetime.datetime.now() - datetime.timedelta(seconds=time_offset))
    else:
        time_offset = 0

    match.append({"grid": grid})
    match.append({"timestamp": {"$gt": time_offset}})

    for player in group:
        match.append({"players": {"$elemMatch": {"name": player}}})

    return collection.find_one({"$and": match}, {"grid": 1, "timestamp": 1}, sort=[("timestamp", -1)])


def find_player_group(group, region, time_offset=None, not_grid=""):
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["grids"]

    match = []

    if time_offset:
        time_offset = datetime.datetime.timestamp(datetime.datetime.now() - datetime.timedelta(seconds=time_offset))
    else:
        time_offset = 0

    match.append({"grid": {"$ne": not_grid}})
    match.append({"timestamp": {"$gt": time_offset}})

    for player in group:
        match.append({"players": {"$elemMatch": {"name": player}}})

    return collection.find_one({"$and": match}, {"grid": 1, "timestamp": 1}, sort=[("timestamp", -1)])


if __name__ == "__main__":
    #pprint(get_player_grids("rince02", "eu", 15000))
    pprint(find_player_group(["rince02", "touharia"], "eu", 200000))





