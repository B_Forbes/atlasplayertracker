import json
from apt_db import get_latest_grids, group_in_grid
import datetime

GROUP_SIZE_MINIMUM = 3
DURATION_GAP = 3

# Number of seconds ship has been on grid
MAX_DURATION = 900


class Player:
    def __init__(self, name, time):
        self.name = name
        self.time = time
        self.used = False


def get_neighboring_coordinates(x, y):
    # Each tile has 8 neighbors
    neighbors = []

    # Up 1, Left 1
    neighbors.append(((x-1) % 15, (y-1) % 15))

    # Up 1
    neighbors.append((x, (y-1) % 15))

    # Up 1, Right 1
    neighbors.append(((x+1) % 15, (y-1) % 15))

    # Left 1
    neighbors.append(((x-1) % 15, y))

    # Right 1
    neighbors.append(((x+1) % 15, y))

    # Down 1, Left 1
    neighbors.append(((x-1) % 15, (y+1) % 15))

    # Down 1
    neighbors.append((x, (y+1) % 15))

    # Down 1, Right 1
    neighbors.append(((x+1) % 15, (y+1) % 15))

    return neighbors


def coordinates_to_grid(coordinates):
    """
    Convert coords to grid. Ex: (0, 0) to A1

    :param coordinates: tuple
    :return: Grid
    """

    x = chr(coordinates[0] + 65)
    y = str(coordinates[1])
    return x+y


def update_ships(region="na"):
    """
    Attempts to get all ships that have crossed into a server
    :return: List of dicts
    """

    grid_ships = {}

    grids = get_latest_grids(region)

    for grid in grids:
        players = []
        groups = []
        try:
            grid_ships[grid["grid"]] = {}
        except:
            continue
        grid_ships[grid["grid"]]["ships"] = []

        for player in grid["players"]:
            if player["duration"] < MAX_DURATION:
                players.append(Player(player["name"], player["duration"]))

        for player in players:
            if player.used:
                continue

            group = [player.name]
            player.used = True

            for p2 in [x for x in players if x.used is False]:
                if abs(player.time - p2.time) <= DURATION_GAP:
                    group.append(p2.name)
                    p2.used = True
            groups.append(group)

        player_count = 0
        for group in groups:

            if len(group) >= GROUP_SIZE_MINIMUM:
                source_grid = confirm_ship(group, grid["grid"], region)
                if source_grid:
                    grid_ships[grid["grid"]]["ships"].append({
                        "players": group,
                        "source_grid": source_grid
                    })
                    player_count += len(group)
        grid_ships[grid["grid"]]["players_on_ships"] = player_count

    data = {
        "meta": {
            "last_updated": int(datetime.datetime.timestamp(datetime.datetime.now()))
        },
        "grids": grid_ships
    }

    f = open("data/{}_pvp_ships.json".format(region), "w")
    f.write(json.dumps(data, indent=4))
    f.close()
    return grid_ships


def confirm_ship(group, grid, region):
    # 65 is ASCII A
    x = ord(grid[0]) - 65
    y = int(grid[1:])

    neighbors = [coordinates_to_grid(neighbor) for neighbor in get_neighboring_coordinates(x, y)]
    # print("Grid: " + grid)
    # print("Group")
    # print(group)
    # print("Neighbors")
    # print(neighbors)

    for neighbor in neighbors:
        group_result = group_in_grid(neighbor, group, region, time_offset=MAX_DURATION)
        if group_result and group_result["grid"] in neighbors:
            return group_result["grid"]
    return False


if __name__ == "__main__":
    update_ships()
