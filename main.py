from flask import request, Flask, Response, send_file
import os

app = Flask(__name__)
os.chdir(os.path.dirname(os.path.abspath(__file__)))


from views.index import view_index
from views.grid import view_grid
from views.search import view_search


@app.route("/")
def index():
    return view_index()


@app.route("/robots.txt")
def robots():
    return send_file("robots.txt")


@app.route("/search")
def search_index():
    return view_search()


@app.route("/player")
def index_player():
    return view_index(view="player")


@app.route("/ship")
def index_ship():
    return view_index(view="ship")


@app.route("/grid/<grid_coordinates>/")
def grid(grid_coordinates):
    return view_grid(grid_coordinates)


@app.route("/eu/")
def index_eu():
    return view_index(region="eu")


@app.route("/eu/search/")
def search_eu_index():
    return view_search(region="eu")


@app.route("/eu/player")
def index_eu_player():
    return view_index(region="eu", view="player")


@app.route("/eu/ship")
def index_eu_ship():
    return view_index(region="eu", view="ship")


@app.route("/eu/grid/<grid_coordinates>/")
def grid_eu(grid_coordinates):
    try:
        return view_grid(grid_coordinates, region="eu")
    except Exception:
        return ":("


@app.route("/na_pvp_players.json")
def na_pvp_players():
    players = open("data/na_pvp_players_dict.json", "r").read()
    return Response(players, mimetype="application/json")


@app.route("/eu_pvp_players.json")
def eu_pvp_players():
    players = open("data/eu_pvp_players_dict.json", "r").read()
    return Response(players, mimetype="application/json")


@app.route("/na_pvp_ships.json")
def na_pvp_ships():
    players = open("data/na_pvp_ships.json", "r").read()
    return Response(players, mimetype="application/json")


@app.route("/eu_pvp_ships.json")
def eu_pvp_ships():
    players = open("data/eu_pvp_ships.json", "r").read()
    return Response(players, mimetype="application/json")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
