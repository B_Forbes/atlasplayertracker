import valve.source.master_server
import valve.source.a2s
import json
import datetime
from pymongo import MongoClient
from update_ship import update_ships

MONGO_HOST = "localhost"
MONGO_PORT = 27017
MINUTES_RETAIN_DATA = 30


def prune_grid_data(region, time):
    """
    Remove all grid records before time, keeps database small and queries fast
    :param region: region
    :param time: minutes
    :return:
    """

    client = MongoClient(MONGO_HOST, MONGO_PORT)
    collection = client["apt_{}".format(region)]["grids"]

    time_offset = datetime.datetime.timestamp(datetime.datetime.now() - datetime.timedelta(minutes=time))

    deletion = collection.delete_many({"timestamp": {"$lt": time_offset}})
    print("Deleted: " + str(deletion.deleted_count))
    return


def update_mongo(data, region):
    client = MongoClient(MONGO_HOST, MONGO_PORT)
    db = client["apt_{}".format(region)]

    grids = db["grids"]
    for grid in data["grids"]:
        grid["timestamp"] = data["meta"]["last_updated"]
        grids.insert_one(grid)

    updates = db["updates"]
    updates.insert_one({"timestamp": data["meta"]["last_updated"]})


def update(region):
    """
    Grabs player information from steam servers and generates the {}_pvp_players.json files

    :param region:
    :return:
    """
    print("Beginning Update [{}]".format(region))
    try:
        data = {"meta": {}, "grids": []}

        servers = json.loads(open("{}_pvp_server_dict.json".format(region), "r").read())
        for server in servers:
            try:
                print(server["grid"])
                host = server["address"]
                port = server["port"]
                with valve.source.a2s.ServerQuerier((host, port)) as s:
                    players = s.players()
                    player_list = []
                    for player in players["players"]:
                        if player["name"]:
                            player_list.append({"name": player["name"],
                                                "duration": player["duration"]})

                    server_dict = {
                        "grid": server["grid"],
                        "players": player_list
                    }

                    if "label" in server.keys():
                        server_dict["label"] = server["label"]

                    data["grids"].append(server_dict)
            except Exception as e:
                print(e)
                pass
        epoch_time = int(datetime.datetime.timestamp(datetime.datetime.now()))
        data["meta"]["last_updated"] = epoch_time
        print("Update Complete [{}] [{}]".format(region, epoch_time))

        f = open("data/{}_pvp_players_dict.json".format(region), "w")
        f.write(json.dumps(data, indent=4))
        f.close()

        print("Updating Mongo...")
        update_mongo(data, region)
        print("Mongo Update Complete")

    except Exception as e:
        print("Exception: " + e)
        pass


if __name__ == "__main__":
    while True:
        update("na")
        print("Updating NA ships...")
        update_ships("na")
        print("Pruning NA data...")
        prune_grid_data("na", MINUTES_RETAIN_DATA)
        update("eu")
        print("Updating EU ships...")
        update_ships("eu")
        print("Pruning NA data...")
        prune_grid_data("eu", MINUTES_RETAIN_DATA)
